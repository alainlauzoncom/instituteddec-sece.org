<?php

/**
 * @file
 * Generate SECE stats.
 */

/**
 * Clears the cache of all sujet_de_conference nodes.
 */
function sece_stats_clear_cache_all_sujets_de_conference() {
  $sujets = db_select('node', 'sdc')->
  fields('sdc', array('nid'))
  ->condition('type', 'sujet_de_conference')
  ->execute();
  
  $ids = array();
  foreach ($sujets as $sujet) {
    $ids[] = $sujet->nid;
  }
  sece_stats_clear_cache_entity('node', $ids);
}

/**
 * Clear cache of selected entities from a certain type.
 *
 * @param string $entity_type
 *               The type of entity.
 * @param array  $ids
 *               An array of entity ids.
 */
function sece_stats_clear_cache_entity($entity_type, $ids) {
    $in = array();
    foreach($ids as $id) {
      $in[] = "field:$entity_type:$id";
    }
    $delete = db_delete("cache_field")
    ->condition('cid', $in, 'IN')
    ->execute();
}

/**
 * Implements hook_page_alter().
 */
function sece_stats_page_alter(&$page) {
  $node = menu_get_object();
  if (!empty($node) && $node->title == 'Envoyer un courriel') {
    if (!empty($_GET) && !empty($_GET['uid'])) {
      $account = user_load($_GET['uid']);
      $element = array(
       // The #tag is the html tag -.
        '#tag' => 'meta',
       // Set up an array of attributes inside the tag.
        '#attributes' => array(
          'http-equiv' => 'refresh',
          'content' => "0;URL=mailto:$account->mail",
        ),
      );
      drupal_add_html_head($element, 'mailto');
      global $user;
      $user = user_load($user->uid);
      $user->field_nombre_de_prises_de_contac[LANGUAGE_NONE][0]['value'] = $user->field_nombre_de_prises_de_contac[LANGUAGE_NONE][0]['value'] + 1;
      user_save($user);
      sece_stats_clear_cache_entity('user', array($user->uid));
    }
  }
}

/**
 * Calculate user stats.
 *
 * @param int $uid
 *   User ID.
 */
function sece_stats_calculate_stats_user($uid) {
  $author = user_load($uid);
  if (!empty($author) && $author !== FALSE) {
    // Calcul du nombre de conférences proposées par un professeur.
    $query = db_select('node', 'sdc');
    $query->fields('sdc', array(
      'nid',
    ));
    $query->innerJoin('field_data_field_type', 'ft', 'ft.entity_id = sdc.nid AND ft.entity_type = \'node\'');
    $query->condition('sdc.uid', $uid);
    $query->condition('sdc.type', 'sujet_de_conference');
    // Publié.
    $query->condition('sdc.status', 1);
    // Offert.
    $query->condition('ft.field_type_tid', 1);
    $nombre_de_conferences_prop = $query->execute()->rowCount();
    $author->field_nombre_de_conferences_prop[LANGUAGE_NONE][0]['value'] = $nombre_de_conferences_prop;

    // Calcul du nombre de conférences données par un professeur.
    $query = db_select('eck_conf_rence', 'conf');
    $query->fields('conf', array(
      'id',
    ));
    $query->innerJoin('field_data_field_confirme', 'fc', 'fc.entity_id = conf.id');
    $query->innerJoin('field_data_field_sujet_de_conference', 'fsdc', 'fsdc.entity_id = conf.id');
    $query->innerJoin('node', 'nsdc', 'nsdc.nid = fsdc.field_sujet_de_conference_target_id');
    $query->condition('nsdc.uid', $uid);
    $query->condition('fc.field_confirme_value', 1);
    $nombre_de_conferences_donn = $query->execute()->rowCount();
    $author->field_nombre_de_conferences_donn[LANGUAGE_NONE][0]['value'] = $nombre_de_conferences_donn;

    // Calcul du nombre de conférences recues par un professeur.
    $query = db_select('eck_conf_rence', 'conf');
    $query->fields('conf', array(
      'id',
    ));
    $query->innerJoin('field_data_field_hote', 'fh', 'fh.entity_id = conf.id AND fh.entity_type = \'conf_rence\' AND fh.bundle = \'conf_rence\'');
    $query->innerJoin('field_data_field_confirme', 'fc', 'fc.entity_id = conf.id AND fh.entity_type = \'conf_rence\' AND fh.bundle = \'conf_rence\'');
    $query->condition('fh.field_hote_target_id', $uid);
    $query->condition('fc.field_confirme_value', 1);
    $nombre_de_conferences_recu = $query->execute()->rowCount();
    $author->field_nombre_de_conferences_recu[LANGUAGE_NONE][0]['value'] = $nombre_de_conferences_recu;

    user_save($author);
  }
}

/**
 * Calculate taxonomy stats.
 */
function sece_stats_calculate_stats_taxonomy() {
  // Number of taxonomy terms per sujet_de_confrence may have changed.
  // Let's recompute those numbers.
  $vocabularies = array(
    'categorie',
    'thematique_d_intervention_privilegiee',
  );
  // Calcul du nombre de conférences proposées par terme de taxonomie.
  $query = db_select('taxonomy_term_data', 'taxonomy_term_data');
  $query->leftJoin('taxonomy_index', 'taxonomy_index', 'taxonomy_term_data.tid = taxonomy_index.tid');
  $query->leftJoin('node', 'node_taxonomy_index', 'taxonomy_index.nid = node_taxonomy_index.nid');
  $query->leftJoin('taxonomy_vocabulary', 'taxonomy_vocabulary', 'taxonomy_term_data.vid = taxonomy_vocabulary.vid');
  $query->leftJoin('field_data_field_type', 'field_data_field_type', 'node_taxonomy_index.nid = field_data_field_type.entity_id');
  $query->fields('taxonomy_term_data', array(
    'name',
    'vid',
    'tid',
  ));
  $query->addExpression('COUNT(DISTINCT node_taxonomy_index.nid)', 'nombre_de_sujets_offerts');
  $query->condition('node_taxonomy_index.type', 'sujet_de_conference');
  // Publié.
  $query->condition('node_taxonomy_index.status', 1);
  // Node.
  $query->condition('field_data_field_type.entity_type', 'node');
  // Offert.
  $query->condition('field_data_field_type.field_type_tid', 1);
  // Vocabulaires.
  $query->condition('taxonomy_vocabulary.machine_name', $vocabularies, 'IN');
  // GROUP BY user ID.
  $query->groupBy('taxonomy_term_data.tid');

  $taxonomy_terms = $query->execute();
  if ($taxonomy_terms) {
    foreach ($taxonomy_terms as $taxonomy_term) {
      $taxonomy_term_object = taxonomy_term_load($taxonomy_term->tid);
      $taxonomy_term_object->field_nombre_de_conferences_offe[LANGUAGE_NONE][0]['value'] = $taxonomy_term->nombre_de_sujets_offerts;
      taxonomy_term_save($taxonomy_term_object);
    }
  }

  // Calcul du nombre de conférences demandées par terme de taxonomie.
  $query = db_select('taxonomy_term_data', 'taxonomy_term_data');
  $query->leftJoin('taxonomy_index', 'taxonomy_index', 'taxonomy_term_data.tid = taxonomy_index.tid');
  $query->leftJoin('node', 'node_taxonomy_index', 'taxonomy_index.nid = node_taxonomy_index.nid');
  $query->leftJoin('taxonomy_vocabulary', 'taxonomy_vocabulary', 'taxonomy_term_data.vid = taxonomy_vocabulary.vid');
  $query->leftJoin('field_data_field_type', 'field_data_field_type', 'node_taxonomy_index.nid = field_data_field_type.entity_id');
  $query->fields('taxonomy_term_data', array(
    'name',
    'vid',
    'tid',
  ));
  $query->addExpression('COUNT(DISTINCT node_taxonomy_index.nid)', 'nombre_de_sujets_demandes');
  $query->condition('node_taxonomy_index.type', 'sujet_de_conference');
  // Publié.
  $query->condition('node_taxonomy_index.status', 1);
  // Node.
  $query->condition('field_data_field_type.entity_type', 'node');
  // Demandée.
  $query->condition('field_data_field_type.field_type_tid', 2);
  // Vocabulaires.
  $query->condition('taxonomy_vocabulary.machine_name', $vocabularies, 'IN');
  // GROUP BY user ID.
  $query->groupBy('taxonomy_term_data.tid');

  $taxonomy_terms = $query->execute();
  if ($taxonomy_terms) {
    foreach ($taxonomy_terms as $taxonomy_term) {
      $taxonomy_term_object = taxonomy_term_load($taxonomy_term->tid);
      $taxonomy_term_object->field_nombre_de_conferences_dema[LANGUAGE_NONE][0]['value'] = $taxonomy_term->nombre_de_sujets_demandes;
      taxonomy_term_save($taxonomy_term_object);
    }
  }

  // Calcul des combinaisons les plus populaires.
  $combinaisons = array();

  // Initialise toutes les combinaisons avec des 0.
  $result = db_query('
  SELECT CONCAT(taxonomy_term_data_champ.name, \' / \', taxonomy_term_data_thematique.name) AS field_pair,
         CONCAT(taxonomy_term_data_champ.tid, \' / \', taxonomy_term_data_thematique.tid) AS combined_tid
  FROM drup_taxonomy_term_data taxonomy_term_data_thematique
  LEFT OUTER JOIN drup_taxonomy_term_data taxonomy_term_data_champ ON taxonomy_term_data_champ.tid > 0
  LEFT OUTER JOIN drup_taxonomy_vocabulary taxonomy_vocabulary_thematique ON taxonomy_term_data_thematique.vid = taxonomy_vocabulary_thematique.vid
  LEFT OUTER JOIN drup_taxonomy_vocabulary taxonomy_vocabulary_champ ON taxonomy_term_data_champ.vid = taxonomy_vocabulary_champ.vid
  WHERE (taxonomy_vocabulary_champ.machine_name =\'categorie\')
    AND (taxonomy_vocabulary_thematique.machine_name =\'thematique_d_intervention_privilegiee\')
  GROUP BY combined_tid
  ORDER BY field_pair
  ');

  $compteur = 0;

  // Result is returned as a iterable object that returns
  // a stdClass object on each iteration.
  foreach ($result as $combinaison) {
    $combinaisons[$combinaison->combined_tid] = array(
      'field_pair' => $combinaison->field_pair,
      'field_nombre_de_sujets_offerts' => 0,
      'field_nombre_de_sujets_demandes' => 0,
    );
    $compteur++;
  }

  // Renseigne le nombre de sujets offerts.
  $result = db_query('
  SELECT CONCAT(taxonomy_term_data_champ.name, \' / \', taxonomy_term_data_thematique.name) AS field_pair,
         CONCAT(taxonomy_term_data_champ.tid, \' / \', taxonomy_term_data_thematique.tid) AS combined_tid,
         COUNT(DISTINCT node_taxonomy_index.nid) AS field_nombre_de_sujets_offerts
  FROM drup_node node_taxonomy_index
  LEFT OUTER JOIN drup_taxonomy_index taxonomy_index_champ ON taxonomy_index_champ.nid = node_taxonomy_index.nid
  LEFT OUTER JOIN drup_taxonomy_term_data taxonomy_term_data_champ ON taxonomy_term_data_champ.tid = taxonomy_index_champ.tid
  LEFT OUTER JOIN drup_taxonomy_vocabulary taxonomy_vocabulary_champ ON taxonomy_term_data_champ.vid = taxonomy_vocabulary_champ.vid
  LEFT OUTER JOIN drup_field_data_field_type field_data_field_type ON node_taxonomy_index.nid = field_data_field_type.entity_id
  LEFT OUTER JOIN drup_node node_taxonomy_index_thematique ON node_taxonomy_index_thematique.nid = node_taxonomy_index.nid
  LEFT OUTER JOIN drup_taxonomy_index taxonomy_index_thematique ON taxonomy_index_thematique.nid = node_taxonomy_index_thematique.nid
  LEFT OUTER JOIN drup_taxonomy_term_data taxonomy_term_data_thematique ON taxonomy_term_data_thematique.tid = taxonomy_index_thematique.tid
  LEFT OUTER JOIN drup_taxonomy_vocabulary taxonomy_vocabulary_thematique ON taxonomy_term_data_thematique.vid = taxonomy_vocabulary_thematique.vid
  WHERE (node_taxonomy_index.type = \'sujet_de_conference\')
    AND (node_taxonomy_index.status = \'1\')
    AND (field_data_field_type.entity_type = \'node\')
    AND (field_data_field_type.field_type_tid = \'1\')
    AND (taxonomy_vocabulary_champ.machine_name IN (\'categorie\'))
    AND (taxonomy_vocabulary_thematique.machine_name IN (\'thematique_d_intervention_privilegiee\'))
  GROUP BY combined_tid
  ');

  // Result is returned as a iterable object that
  // returns a stdClass object on each iteration.
  foreach ($result as $combinaison) {
    $combinaisons[$combinaison->combined_tid]['field_nombre_de_sujets_offerts'] = $combinaison->field_nombre_de_sujets_offerts;
  }

  // Renseigne le nombre de sujets demandes.
  $result = db_query('
  SELECT CONCAT(taxonomy_term_data_champ.name, \' / \', taxonomy_term_data_thematique.name) AS field_pair,
         CONCAT(taxonomy_term_data_champ.tid, \' / \', taxonomy_term_data_thematique.tid) AS combined_tid,
         COUNT(DISTINCT node_taxonomy_index.nid) AS field_nombre_de_sujets_demandes
  FROM drup_node node_taxonomy_index
  LEFT OUTER JOIN drup_taxonomy_index taxonomy_index_champ ON taxonomy_index_champ.nid = node_taxonomy_index.nid
  LEFT OUTER JOIN drup_taxonomy_term_data taxonomy_term_data_champ ON taxonomy_term_data_champ.tid = taxonomy_index_champ.tid
  LEFT OUTER JOIN drup_taxonomy_vocabulary taxonomy_vocabulary_champ ON taxonomy_term_data_champ.vid = taxonomy_vocabulary_champ.vid
  LEFT OUTER JOIN drup_field_data_field_type field_data_field_type ON node_taxonomy_index.nid = field_data_field_type.entity_id
  LEFT OUTER JOIN drup_node node_taxonomy_index_thematique ON node_taxonomy_index_thematique.nid = node_taxonomy_index.nid
  LEFT OUTER JOIN drup_taxonomy_index taxonomy_index_thematique ON taxonomy_index_thematique.nid = node_taxonomy_index_thematique.nid
  LEFT OUTER JOIN drup_taxonomy_term_data taxonomy_term_data_thematique ON taxonomy_term_data_thematique.tid = taxonomy_index_thematique.tid
  LEFT OUTER JOIN drup_taxonomy_vocabulary taxonomy_vocabulary_thematique ON taxonomy_term_data_thematique.vid = taxonomy_vocabulary_thematique.vid
  WHERE (node_taxonomy_index.type = \'sujet_de_conference\')
    AND (node_taxonomy_index.status = \'1\')
    AND (field_data_field_type.entity_type = \'node\')
    AND (field_data_field_type.field_type_tid = \'2\')
    AND (taxonomy_vocabulary_champ.machine_name IN (\'categorie\'))
    AND (taxonomy_vocabulary_thematique.machine_name IN (\'thematique_d_intervention_privilegiee\'))
  GROUP BY combined_tid
  ');

  // Result is returned as a iterable object that
  // returns a stdClass object on each iteration.
  foreach ($result as $combinaison) {
    $combinaisons[$combinaison->combined_tid]['field_nombre_de_sujets_demandes'] = $combinaison->field_nombre_de_sujets_demandes;
  }

  // Détruire tous les records existants.
  $entityType = 'sece_nombre_de_sujets_par_pair';
  $results = entity_load($entityType);
  $entity_ids = array_keys($results);
  $result = entity_delete_multiple($entityType, $entity_ids);

  foreach ($combinaisons as $combinaison) {
    sece_stats_add_nombre_de_sujets_par_pair($combinaison);
  }
}

/**
 * Calculate the number of given conference for one or all sujet_de_conference.
 * @param int $sujet_nid
 */
function sece_stats_calculate_stats_sujet_de_conference($sujet_nid = NULL) {
  if ($sujet_nid === NULL) {
    // Sets all to 0 in the
    db_delete('field_data_field_donnee')
    ->condition('bundle', 'sujet_de_conference')
    ->condition('entity_type', 'node')
    ->execute();
  }

  // Build the SELECT query.
  $query = db_select('node', 'sdc');
  $query->join('field_data_field_sujet_de_conference', 'fsdc', 'fsdc.field_sujet_de_conference_target_id = sdc.nid');
  $query->join('field_data_field_confirme', 'fc', 'fc.entity_id = fsdc.entity_id');
  $query->addExpression("'node'", 'entity_type');
  $query->addExpression("'sujet_de_conference'", 'bundle');
  $query->addExpression("'0'", 'deleted');
  $query->addField('sdc', 'nid', 'entity_id');
  $query->addField('sdc', 'nid', 'revision_id');
  $query->addExpression("'und'", 'language');
  $query->addExpression("'0'", 'delta');
  $query->addExpression('COUNT(fc.field_confirme_value)', 'field_donnee_value');
  $query->condition('fc.field_confirme_value', '1');
  $query->groupBy('sdc.nid');
  
  // Perform the insert.
  db_insert('field_data_field_donnee')
  ->from($query)
  ->execute();

  sece_stats_clear_cache_all_sujets_de_conference();
}

/**
 * Implements hook_cron().
 */
function sece_stats_cron() {
  // Default to an hourly interval. Of course, cron has
  // to be running at least
  // hourly for this to work.
  //$interval = variable_get('sece_stats_cron_interval', 60 * 60);
  $interval = variable_get('sece_stats_cron_interval', 0);
  // We usually don't want to act every time cron runs (which could be every
  // minute) so keep a time for the next run in a variable.
  if (time() >= variable_get('sece_stats_cron_next_execution', 0)) {
    // Calculate stats for users.
    $professeurs = sece_stats_get_users_with_role('professeur', TRUE);
    foreach ($professeurs as $professeur) {
      //sece_stats_calculate_stats_user($professeur->uid);
    }
    //sece_stats_calculate_stats_taxonomy();
    sece_stats_calculate_stats_sujet_de_conference();
    watchdog('sece_stats_cron', 'Calcul des statistiques réussi.');
    variable_set('sece_stats_cron_execution', time() + $interval);
  }
}

/**
 * Return all users who have the given role.
 *
 * @param int|string $role
 *   Name of the role or the ID or the role.
 * @param bool|true $active_user
 *   Determine, if only the active users should be returned.
 *
 * @return array
 *   Array of user objects.
 */
function sece_stats_get_users_with_role($role, $active_user = TRUE) {
  $users = array();
  $rid = 0;
  if (is_int($role)) {
    $rid = $role;
  }
  else {
    if ($role_obj = user_role_load_by_name($role)) {
      $rid = $role_obj->rid;
    }
  }
  if ($rid) {
    $uids = db_select('users_roles', 'ur')->fields('ur', array(
      'uid',
    ))->condition('ur.rid', $rid)->execute()->fetchCol();
    if (!empty($uids)) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'user')->propertyCondition('uid', $uids, 'IN');
      if ($active_user) {
        $query->propertyCondition('status', 1);
      }
      $entities = $query->execute();
      if (!empty($entities)) {
        $users = user_load_multiple(array_keys($entities['user']));
      }
    }
  }

  return $users;
}

/**
 * Add an entity that contains the number of subjects per pair.
 *
 * @param array $combinaison
 *            The data to save into the entity.
 */
function sece_stats_add_nombre_de_sujets_par_pair($combinaison) {
  $combinaison_array = array(
    'type' => 'sece_nombre_de_sujets_par_pair',
    'bundle' => 'sece_nombre_de_sujets_par_pair',
    'field_pair' => array(
      LANGUAGE_NONE => array(
        0 => array(
          'value' => $combinaison['field_pair'],
          'format' => NULL,
          'safe_value' => $combinaison['field_pair'],
        ),
      ),
    ),
    'field_nombre_de_sujets_offerts' => array(
      LANGUAGE_NONE => array(
        0 => array(
          'value' => $combinaison['field_nombre_de_sujets_offerts'],
        ),
      ),
    ),
    'field_nombre_de_sujets_demandes' => array(
      LANGUAGE_NONE => array(
        0 => array(
          'value' => $combinaison['field_nombre_de_sujets_demandes'],
        ),
      ),
    ),
  );
  entity_save('sece_nombre_de_sujets_par_pair', ( object ) $combinaison_array);
}

/**
 * Recalculate some stats depending on some transitions.
 *
 * @param object $node
 *            Node that will be checked for transitions.
 * @param bool $previous_state
 *            Previous state of the node.
 * @param bool $new_state
 *            New state of the node.
 */
function sece_stats_workbench_moderation_transition($node, $previous_state, $new_state) {
  if ($new_state == 'published' && $node->type = 'sujet_de_conference') {
    sece_stats_calculate_stats_user($node->uid);
  }
}
