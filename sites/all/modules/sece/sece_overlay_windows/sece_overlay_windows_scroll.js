/**
 * @file
 * Resize scrolling regions when needed.
 */

Drupal.behaviors.SeceScroll = {
  attach: function (context, settings) {
    jQuery(window).resize(function () {
      seceScrollResize();
    });
    function seceScrollResize() {
      if (window.innerWidth > 844 && jQuery('body.page-conferences-proposees, body.page-conferences-recherchees').length > 0) {
        heightEi = jQuery('#environment-indicator').outerHeight(true);
        heightSh = jQuery('#section-header').outerHeight(true);
        heightLst = jQuery('#left-sidebar-title').outerHeight(true);
        heightFooter = jQuery('#section-user-4').outerHeight(true);
        heightViewPort = window.innerHeight;
        heightRight = heightViewPort - (heightEi + heightSh + heightLst + heightFooter);
        heightLeft = heightRight;
        jQuery('.region-content').css('height', heightRight);
        jQuery('.region.region-left-sidebar').css('height', heightLeft);
      }
      else {
        jQuery('.region-content').css('margin-bottom', jQuery('#section-user-4').outerHeight(true) + 10);
      }
    }
    setTimeout(seceScrollResize, 150);
  }
};
