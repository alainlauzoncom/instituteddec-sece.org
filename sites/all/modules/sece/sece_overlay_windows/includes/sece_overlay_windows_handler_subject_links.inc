<?php

/**
 * @file
 * Provides moderation links for Views.
 */

/**
 * Extends views_handler_field.
 */
class SeceOverlayWindowsHandlerSubjectLinks extends views_handler_field {

  /**
   * Overrides views_handler_field::render.
   *
   * {@inheritDoc}
   *
   * @see views_handler_field::render()
   */
  public function render($values) {
    $node = node_load($values->{$this->aliases['nid']});
    return theme('links', array(
      'links' => sece_overlay_windows_get_subject_links($node, array(
        'html' => TRUE,
        'query' => array(
          'destination' => $_GET['q'],
        ),
      )),
    ));
  }

}
