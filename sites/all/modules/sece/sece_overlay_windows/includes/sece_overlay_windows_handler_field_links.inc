<?php

/**
 * @file
 * Provides moderation links for Views.
 */

/**
 * Extends views_handler_field.
 */
class SeceOverlayWindowsHandlerFieldLinks extends views_handler_field {

  /**
   * Overrides views_handler_field::render.
   *
   * {@inheritDoc}
   *
   * @see views_handler_field::render()
   */
  public function render($values) {
    if ($values->{$this->aliases['is_current']}) {
      $node = node_load($values->{$this->aliases['nid']}, $values->{$this->aliases['vid']});
      return theme('links', array(
        'links' => sece_overlay_windows_get_moderation_links($node, array(
          'html' => TRUE,
          'query' => array(
            'destination' => $_GET['q'],
          ),
        )),
      ));
    }
    return '';
  }

}
