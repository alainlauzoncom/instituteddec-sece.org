<?php

/**
 * @file
 * Content moderation views integration for SECE.
 */

/**
 * Implements hook_views_data_alter().
 */
function sece_overlay_windows_views_data_alter(&$data) {
  if (!empty($data['workbench_moderation_node_history'])) {
    $data['workbench_moderation_node_history']['moderation_actions']['field']['handler'] = 'SeceOverlayWindowsHandlerFieldLinks';
  }
}

/**
 * Implements hook_views_data().
 */
function sece_overlay_windows_views_data() {
  $data['node']['sece_sujets_selectionnes_actions'] = array(
    'title' => t('Actions de sujets sélectionés'),
    'help' => t('Contient les actions possibles pour les sujets de conférences présélectionés.'),
    'field' => array(
      'title' => t('Liens des sujets présélectionnés'),
      'handler' => 'SeceOverlayWindowsHandlerFieldLinks',
      'click sortable' => FALSE,
      'additional fields' => array(
        'nid',
      ),
    ),
  );

  return $data;
}
