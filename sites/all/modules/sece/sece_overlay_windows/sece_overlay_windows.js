/**
 * @file
 * Move some labels.
 */

Drupal.behaviors.SeceSearchEngineMultiSelectWidget = {
  attach: function (context, settings) {
    jQuery("#title-field-add-more-wrapper .description").detach().insertAfter('#title-field-add-more-wrapper label:not(.option)');
    jQuery("#edit-field-categories .description").detach().insertAfter('#edit-field-categories label:not(.option)');
    jQuery("#edit-field-thematiques-d-intervention .description").detach().insertAfter('#edit-field-thematiques-d-intervention label:not(.option)');
  }
};
