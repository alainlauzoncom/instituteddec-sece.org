/**
 * @file
 */

Drupal.behaviors.SeceSearchEngineMultiSelectWidget = {
  attach: function (context, settings) {
    function resizeSelectWidget(chosenContainer, parent) {
      chosenContainer.css('width', '30px');
      var width = Math.min(parent.width() - 12, 221);
      chosenContainer.css('width', width);
    }
    function resizeInputLabel(label) {
      label.css('width', '1px');
      var parent = label.parent();
      var width = Math.min(parent.width() - 12, 221);
      label.css('width', width);
    }
    settings.compactForms.forms.forEach(function (element) {
      jQuery('#' + element + ' select').each(function (index, element) {
        var text = '';
        switch (element.id) {
          case "edit-field-categories-tid":
            text = "Champs d'expertise";
            break;

          case "edit-field-thematiques-d-intervention-tid":
            text = "Thématiques";
            break;
        }
        jQuery(element).chosen({
          placeholder_text_multiple: text
        });
        var chosenContainer = jQuery(element).next();
        var parent = chosenContainer.parent().parent().parent().parent().parent();
        resizeSelectWidget(chosenContainer, parent);
      });
      jQuery('#' + element + ' select').on('change', function (evt, params) {
        var text = '';
        switch (this.id) {
          case "edit-field-categories-tid":
            text = "Champs d'expertise";
            break;

          case "edit-field-thematiques-d-intervention-tid":
            text = "Thématiques";
            break;
        }
        jQuery(this).next().find('li.search-field').html(text);
      });
    });
    jQuery(window).resize(function () {
      jQuery('form.compact-form select').each(function (index, element) {
        var chosenContainer = jQuery(element).next();
        var parent = chosenContainer.parent().parent().parent().parent().parent();
        resizeSelectWidget(chosenContainer, parent);
      });
      jQuery('.region-left-sidebar .compact-form-wrapper label').each(function (index, element) {
        resizeInputLabel(jQuery(element));
      });
    });
    jQuery('.region-left-sidebar .compact-form-wrapper label').each(function (index, element) {
      resizeInputLabel(jQuery(element));
    });
  }
};
